package net.juyttersprot.www.rest;

import net.juyttersprot.www.backend.TypeService;
import net.juyttersprot.www.model.Type;
import net.juyttersprot.www.rest.controller.TypeController;
import net.juyttersprot.www.rest.form.TypeForm;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TypeControllerTest {

    private TypeService typeService = mock(TypeService.class);
    private TypeController typeController;

    @Before
    public void setup() {
        typeController = new TypeController(typeService);
    }

    @Test
    public void testGetTypes() {
        List<Type> returnTypes = Arrays.asList(new Type("a"), new Type("b"));

        when(typeService.getTypes()).thenReturn(returnTypes);

        List<Type> actualReturnedTyped = typeController.getTypes();
        assertThat(actualReturnedTyped).isEqualTo(returnTypes);
        verify(typeService, times(1)).getTypes();

    }

    @Test
    public void testCreateType() {
        typeController.createType(new TypeForm("Bla"));
        verify(typeService, times(1)).createType("Bla");
    }
}
