package net.juyttersprot.www.integration;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import net.juyttersprot.www.ShivaApplication;
import net.juyttersprot.www.model.Type;
import net.juyttersprot.www.repository.TypeRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ShivaApplication.class)
@WebAppConfiguration
public class TypeControllerIntegrationTest {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private TypeRepository typeRepo;

    @Before
    public void setup() {

        typeRepo.deleteAll();
        typeRepo.save(new Type("test1"));
        typeRepo.save(new Type("test2"));
        typeRepo.save(new Type("test3"));

        this.mockMvc = webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void getTypes() throws Exception {

        MvcResult mvcResult = mockMvc.perform(get("/rest/types"))
                .andExpect(status().isOk())
                .andReturn();

        String mvcContent = mvcResult.getResponse().getContentAsString();

        assertThat(mvcContent.contains("test1")).isTrue();
        assertThat(mvcContent.contains("test2")).isTrue();
        assertThat(mvcContent.contains("test3")).isTrue();


    }

    @Test
    public void getTypeById() throws Exception {
        //get the list
        MvcResult mvcResultFromFindAll = mockMvc.perform(get("/rest/types"))
                .andExpect(status().isOk())
                .andReturn();
        List<Type> blah = convertFromJson(mvcResultFromFindAll.getResponse().getContentAsString());

        MvcResult mvcResult = mockMvc.perform(get("/rest/types/" + blah.get(0).getId()))
                .andExpect(status().isOk())
                .andReturn();

        String mvcContent = mvcResult.getResponse().getContentAsString();
        assertThat(mvcContent.contains(blah.get(0).getValue())).isTrue();

    }

    @Test
    public void testDelete() throws Exception {
        //get the list
        MvcResult mvcResultFromFindAll = mockMvc.perform(get("/rest/types"))
                .andExpect(status().isOk())
                .andReturn();
        List<Type> types = convertFromJson(mvcResultFromFindAll.getResponse().getContentAsString());

        mockMvc.perform(delete("/rest/types/{id}", types.get(0).getId()))
                .andExpect(status().isOk());

        mvcResultFromFindAll = mockMvc.perform(get("/rest/types"))
                .andExpect(status().isOk())
                .andReturn();
        List<Type> newTypes = convertFromJson(mvcResultFromFindAll.getResponse().getContentAsString());
        assertThat(newTypes.size()).isEqualTo(2);
    }

    private List<Type> convertFromJson(final String json) throws IOException {
        ObjectMapper mapper = new ObjectMapper();

        return mapper.readValue(json, TypeFactory.defaultInstance().constructCollectionType(List.class, Type.class));
    }


}
