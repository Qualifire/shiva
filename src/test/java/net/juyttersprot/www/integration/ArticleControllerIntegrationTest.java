package net.juyttersprot.www.integration;

import net.juyttersprot.www.ShivaApplication;
import net.juyttersprot.www.model.Article;
import net.juyttersprot.www.repository.ArticleRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ShivaApplication.class)
@WebAppConfiguration
public class ArticleControllerIntegrationTest {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private ArticleRepository articleRepo;

    @Before
    public void setup(){

        this.mockMvc = webAppContextSetup(webApplicationContext).build();

        articleRepo.save(new Article("WINTER2015","broek","mexx","kutbroek",new BigDecimal(15.1),null, null,null));
        articleRepo.save(new Article("WINTER2015","rok","soleil d'or","bruine rok",new BigDecimal(150.15),null, null,null));
        articleRepo.save(new Article("ZOMER2014","pull","raar merk","schone blauwe pull",new BigDecimal(99.9),null, null,null));
        articleRepo.save(new Article("LENTE2015","schoenen","Trippen","Boots of war",new BigDecimal(120),null, null,null));
        articleRepo.save(new Article("HERFST2015","juwelen","bos","dubbele ring",new BigDecimal(325.21),null, null,null));
    }

    @Test
    public void testFindAll() throws Exception {

        MvcResult mvcResult = mockMvc.perform(get("/rest/articles"))
                .andExpect(status().isOk())
                .andReturn();

        String mvcContent = mvcResult.getResponse().getContentAsString();

        System.out.println(mvcContent);

    }
}
