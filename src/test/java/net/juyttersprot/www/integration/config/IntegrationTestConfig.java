package net.juyttersprot.www.integration.config;

import com.mongodb.Mongo;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import cz.jirutka.spring.embedmongo.EmbeddedMongoBuilder;
import de.flapdoodle.embed.mongo.distribution.Version;

import java.io.IOException;

@Configuration
public class IntegrationTestConfig {

    @Bean(destroyMethod = "close")
    public Mongo mongo() throws IOException {
        return new EmbeddedMongoBuilder()
                .version(Version.V2_7_1)
                .bindIp("127.0.0.1")
                .build();
    }
}
