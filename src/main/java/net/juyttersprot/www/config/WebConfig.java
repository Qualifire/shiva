package net.juyttersprot.www.config;

import net.juyttersprot.www.security.RequestLogger;
import org.springframework.boot.context.embedded.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.filter.CompositeFilter;

import java.util.Arrays;

@Configuration
public class WebConfig {

    @Bean
    public FilterRegistrationBean filterRegistrationBean() {
        CompositeFilter filter = new CompositeFilter();
        filter.setFilters(Arrays.asList(new RequestLogger()
        ));
        return new FilterRegistrationBean(filter);
    }


}
