package net.juyttersprot.www.backend;

import net.juyttersprot.www.model.Type;
import net.juyttersprot.www.repository.TypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TypeService {

    TypeRepository typeRepo;

    @Autowired
    public TypeService(final TypeRepository typeRepo) {
        this.typeRepo = typeRepo;
    }

    public List<Type> getTypes() {
        return typeRepo.findAll();
    }

    public Type createType(final String type) {
        return typeRepo.save(new Type(type));
    }

    public Type getType(final String id) {
        return typeRepo.findOne(id);
    }

    public Type updateType(final String id, final String value) {
        return typeRepo.save(new Type(id, value));
    }

    public void deleteType(final String id) {
        typeRepo.delete(id);
    }
}
