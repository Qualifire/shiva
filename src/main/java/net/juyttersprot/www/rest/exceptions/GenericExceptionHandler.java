package net.juyttersprot.www.rest.exceptions;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GenericExceptionHandler {

    @ExceptionHandler(EmptyRequestBodyException.class)
    public ResponseEntity<?> handleEmptyRequestBodyException(final EmptyRequestBodyException e) {
        return ResponseEntity.badRequest().build();
    }


}
