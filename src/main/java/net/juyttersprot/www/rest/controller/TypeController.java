package net.juyttersprot.www.rest.controller;

import net.juyttersprot.www.backend.TypeService;
import net.juyttersprot.www.model.Type;
import net.juyttersprot.www.rest.exceptions.EmptyRequestBodyException;
import net.juyttersprot.www.rest.form.TypeForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "rest/types")
public class TypeController {


    private TypeService typeService;

    @Autowired
    public TypeController(final TypeService typeService) {
        this.typeService = typeService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<Type> getTypes() {
        return typeService.getTypes();
    }

    @RequestMapping(value = "{typeId}", method = RequestMethod.GET)
    public Type getType(@PathVariable(value = "typeId") final String typeId) {
        return typeService.getType(typeId);
    }

    @RequestMapping(method = RequestMethod.PUT)
    public ResponseEntity<?> updateType(@RequestBody @Valid TypeForm typeForm) {
        notNullRequestBody(typeForm.getId());
        notNullRequestBody(typeForm.getValue());

        typeService.updateType(typeForm.getId(), typeForm.getValue());

        return ResponseEntity.ok().build();
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<?> createType(@RequestBody @Valid TypeForm typeForm) {
        notNullRequestBody(typeForm.getValue());

        typeService.createType(typeForm.getValue());

        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable String id) {
        typeService.deleteType(id);

    }

    private void notNullRequestBody(Object body) {
        if (body == null) {
            throw new EmptyRequestBodyException();
        }
    }
}
