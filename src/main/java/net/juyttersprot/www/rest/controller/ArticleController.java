package net.juyttersprot.www.rest.controller;

import net.juyttersprot.www.backend.ArticleService;
import net.juyttersprot.www.model.Article;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "rest/articles")
public class ArticleController {

    private ArticleService articleService;

    @Autowired
    public ArticleController(final ArticleService articleService) {
        this.articleService = articleService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<Article> getArticles() {
        return articleService.getArticles();
    }
}
