package net.juyttersprot.www.rest.form;

public class TypeForm {
    private String id;
    private String value;

    TypeForm() {
    }

    public TypeForm(final String id, final String value) {
        this.id = id;
        this.value = value;
    }

    public TypeForm(final String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public String getId() {
        return id;
    }
}


