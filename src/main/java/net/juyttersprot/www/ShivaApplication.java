package net.juyttersprot.www;

import net.juyttersprot.www.model.Article;
import net.juyttersprot.www.model.Type;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.math.BigDecimal;

import static java.math.RoundingMode.HALF_EVEN;

@SpringBootApplication
public class ShivaApplication {

    @Autowired
    private MongoTemplate mongoTemplate;

    public static void main(String[] args) {
        SpringApplication.run(ShivaApplication.class, args);
    }

    @Bean
    public InitializingBean initTypes() {
        return () -> {
            /*mongoTemplate.dropCollection(Type.class);
            mongoTemplate.save(new Type("testinit1"));
            mongoTemplate.save(new Type("testinit2"));
            mongoTemplate.save(new Type("testinit3"));*/

            mongoTemplate.dropCollection(Article.class);
            mongoTemplate.save(new Article("WINTER2015", "broek", "mexx", "kutbroek", new BigDecimal(15.1).setScale(2, HALF_EVEN), null, null, null));
            mongoTemplate.save(new Article("WINTER2015", "rok", "soleil d'or", "bruine rok", new BigDecimal(150.15).setScale(2, HALF_EVEN), null, null, null));
            mongoTemplate.save(new Article("ZOMER2014", "pull", "raar merk", "schone blauwe pull", new BigDecimal(99.9).setScale(2, HALF_EVEN), null, null, null));
            mongoTemplate.save(new Article("LENTE2015", "schoenen", "Trippen", "Boots of war", new BigDecimal(120).setScale(2, HALF_EVEN), null, null, null));
            mongoTemplate.save(new Article("HERFST2015", "juwelen", "bos", "dubbele ring", new BigDecimal(325.21).setScale(2, HALF_EVEN), null, null, null));
        };
    }
}
