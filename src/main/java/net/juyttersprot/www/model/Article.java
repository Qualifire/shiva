package net.juyttersprot.www.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;
import java.text.DateFormat;

@Document(collection = "articles")
public class Article {

    @Id
    private String id;
    private String season;
    private String type;
    private String brand;
    private String description;
    private BigDecimal unitPrice;
    private Long createdOn;
    private BigDecimal unitDiscountPercentage;
    private BigDecimal fullCardDiscount;
    private BigDecimal soldPrice;

    Article() {
    }

    public Article(final String season, final String type, final String brand, final String description, final BigDecimal unitPrice, final BigDecimal unitDiscountPercentage, final BigDecimal fullCardDiscount, final BigDecimal soldPrice) {
        this.season = season;
        this.type = type;
        this.brand = brand;
        this.description = description;
        this.unitPrice = unitPrice;
        this.createdOn = System.currentTimeMillis();
        this.unitDiscountPercentage = unitDiscountPercentage;
        this.fullCardDiscount = fullCardDiscount;
        this.soldPrice = soldPrice;
    }

    public String getId() {
        return id;
    }

    public String getSeason() {
        return season;
    }

    public String getType() {
        return type;
    }

    public String getBrand() {
        return brand;
    }

    public String getDescription() {
        return description;
    }

    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public Long getCreatedOn() {
        return createdOn;
    }

    public BigDecimal getUnitDiscountPercentage() {
        return unitDiscountPercentage;
    }

    public BigDecimal getFullCardDiscount() {
        return fullCardDiscount;
    }

    public BigDecimal getSoldPrice() {
        return soldPrice;
    }

    public String getDateTime() {
        return DateFormat.getDateInstance(DateFormat.SHORT).format(createdOn);
    }
}
