package net.juyttersprot.www.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "brands")
public class Brand {

    @Id
    private String id;
    private String value;

    Brand(){

    }

    public Brand(final String id, final String value) {
        this.id = id;
        this.value = value;
    }

    public Brand(final String value) {
        this.value = value;
    }

    public String getId() {
        return id;
    }

    public String getValue() {
        return value;
    }
}
