package net.juyttersprot.www.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "types")
public class Type {

    @Id
    private String id;
    private String value;

    Type(){

    }

    public Type(final String id, final String value) {
        this.id = id;
        this.value = value;
    }

    public Type(final String value) {
        this.value = value;
    }

    public String getId() {
        return id;
    }

    public String getValue() {
        return value;
    }
}
