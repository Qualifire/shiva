package net.juyttersprot.www.security;

import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class RequestLogger extends GenericFilterBean {

    @Override
    public void doFilter(final ServletRequest servletRequest, final ServletResponse servletResponse, final FilterChain filterChain) throws IOException, ServletException {
        if (servletRequest instanceof HttpServletRequest && servletResponse instanceof HttpServletResponse) {
            System.out.println("!!!!!!!!!!!!!!!!!!request is " + servletRequest.getClass());
            System.out.println("!!!!!!!!!!!!!!!!!!request URL : " + ((HttpServletRequest) servletRequest).getRequestURL());
            System.out.println("!!!!!!!!!!!!!!!!!!response is " + servletResponse.getClass());
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }
}
