package net.juyttersprot.www.repository;

import net.juyttersprot.www.model.Article;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ArticleRepository extends MongoRepository<Article, String>{
}
