package net.juyttersprot.www.repository;

import net.juyttersprot.www.model.Type;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface TypeRepository extends MongoRepository<Type, String> {

    Type findOne(String id);

    Type save(Type saved);

    void delete(Type deleted);



}
